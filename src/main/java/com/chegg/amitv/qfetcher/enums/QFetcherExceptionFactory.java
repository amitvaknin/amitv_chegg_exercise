package com.chegg.amitv.qfetcher.enums;

public enum QFetcherExceptionFactory {

    UNABLE_TO_READ_FILE(400,"Unable to read from the following file [%s]"),
    UNABLE_TO_READ_URL(400,"Unable to read from the following url [%s]"),
    DATA_SOURCE_TYPE_IS_NOT_SUPPORTED(400,"Data source type [%s] doesn't supported"),
    FETCH_QUESTIONS_TIME_OUT(400, "Timeout: The api call takes more than [%d] seconds"),
    UNABLE_TO_COMPLETE_YOUR_REQUEST(400, "Unable to complete your request, message: [%s]");


    private int errorCode;
    private String message;

    private QFetcherExceptionFactory(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}