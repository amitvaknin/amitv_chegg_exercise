package com.chegg.amitv.qfetcher.enums;

public enum QuestionSourceType {
    CSV("(?i)^text/csv$"), JSON("(?i)^application/json$"), IMAGE("(?i)^image/.*$");

    private String regexMatcher;
    private QuestionSourceType(String regexMatcher) {
        this.regexMatcher = regexMatcher;
    }

    public static QuestionSourceType getQuestionSourceTypeByMimeType(String mimeType) {
        for (QuestionSourceType questionSourceType : values()) {
            if (mimeType.matches(questionSourceType.regexMatcher)) {
                return questionSourceType;
            }
        }

        return null;
    }
}
