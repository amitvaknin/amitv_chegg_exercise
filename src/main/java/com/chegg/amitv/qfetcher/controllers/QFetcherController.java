package com.chegg.amitv.qfetcher.controllers;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import com.chegg.amitv.qfetcher.exceptions.QFetcherException;
import com.chegg.amitv.qfetcher.models.FetchQuestionsRequest;
import com.chegg.amitv.qfetcher.models.Question;
import com.chegg.amitv.qfetcher.services.QFetcherService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.*;

@RestController
public class QFetcherController {
    private final QFetcherService qFetcherService;

    @Value("${com.chegg.amitv.qfetcher.timeout}")
    private int timeOut;

    @Autowired
    public QFetcherController(QFetcherService qFetcherService) {
        this.qFetcherService = qFetcherService;
    }

    @PostMapping("/api/v1/fetch")
    public List<Question> fetchQuestions(@RequestBody FetchQuestionsRequest fetchQuestionsRequest)  {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<List<Question>> future = executorService.submit(() -> qFetcherService.fetch(fetchQuestionsRequest));
        try {
            List<Question> questions = future.get(timeOut, TimeUnit.MILLISECONDS);
            executorService.shutdown();
            return questions;
        } catch (InterruptedException | TimeoutException e) {
            throw new QFetcherException(QFetcherExceptionFactory.FETCH_QUESTIONS_TIME_OUT, timeOut);
        } catch (ExecutionException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_COMPLETE_YOUR_REQUEST, ExceptionUtils.getRootCauseMessage(e));
        }
    }
}
