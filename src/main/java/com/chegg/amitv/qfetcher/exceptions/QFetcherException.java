package com.chegg.amitv.qfetcher.exceptions;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;

public class QFetcherException extends RuntimeException {
    private int errorCode;
    public QFetcherException(QFetcherExceptionFactory qFetcherExceptionFactory, Object ... params) {
        super(String.format(qFetcherExceptionFactory.getMessage(), params));
        this.errorCode = qFetcherExceptionFactory.getErrorCode();
        printStackTrace();
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "QFetcherException{" +
                "errorCode=" + errorCode +
                '}';
    }
}
