package com.chegg.amitv.qfetcher.exceptions;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(QFetcherException.class)
    public ResponseEntity<Object> QFetcherExceptionHandler(QFetcherException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.valueOf(exception.getErrorCode()));
    }
}
