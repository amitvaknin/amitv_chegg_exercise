package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import com.chegg.amitv.qfetcher.enums.QuestionSourceType;
import com.chegg.amitv.qfetcher.exceptions.QFetcherException;
import com.chegg.amitv.qfetcher.models.DataSource;
import com.chegg.amitv.qfetcher.models.FetchQuestionsRequest;
import com.chegg.amitv.qfetcher.models.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
public class QFetcherService {
    @Value("${com.chegg.amitv.qfetcher.timeout}")
    private int timeOut;

    public static final int THREAD_POOL_SIZE = 10;
    private final QFetcherHandlerProvider qFetcherHandlerProvider;

    @Autowired
    public QFetcherService(QFetcherHandlerProvider qFetcherHandlerProvider) {
        this.qFetcherHandlerProvider = qFetcherHandlerProvider;
    }

    public List<Question> fetch(FetchQuestionsRequest fetchQuestionsRequest) throws ExecutionException, InterruptedException {
        Set<DataSource> dataSources = getDataSources(fetchQuestionsRequest);
        dataSources = filterDataSources(dataSources, fetchQuestionsRequest);
        return fetchQuestionsMultiThreads(dataSources);
    }

    private List<Question> fetchQuestionsMultiThreads(Set<DataSource> dataSources) throws InterruptedException, ExecutionException {
        List<Question> questions = Collections.synchronizedList(new ArrayList<>());
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

        List<Callable<List<Question>>> callables = new ArrayList<>();
        for (DataSource dataSource : dataSources) {
            callables.add(() -> fetchDataSource(dataSource));
        }

        List<Future<List<Question>>> futures = executorService.invokeAll(callables, timeOut, TimeUnit.SECONDS);
        for (Future<List<Question>> future : futures) {
            questions.addAll(future.get());
        }

        executorService.shutdown();
        return questions;
    }

    private List<Question> fetchDataSource(DataSource dataSource) {
        QFetcherHandler dataSourceFetcher = qFetcherHandlerProvider.getHandler(dataSource.getQuestionSourceType());
        return dataSourceFetcher.fetch(dataSource.getUrl());
    }

    private Set<DataSource> getDataSources(FetchQuestionsRequest fetchQuestionsRequest) {
        URL manifestUrl = getUrl(fetchQuestionsRequest.getManifest());
        Set<DataSource> dataSources = new HashSet<>();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(manifestUrl.openStream()))){
            bufferedReader.lines().forEach(dataSourceUrl -> {
                if (!StringUtils.isEmpty(dataSourceUrl)) {
                    dataSources.add(readManifestFileDataSource(dataSourceUrl));
                }
            });
        } catch (IOException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_URL, fetchQuestionsRequest.getManifest());
        }

        return dataSources;
    }

    private Set<DataSource> filterDataSources(Set<DataSource> dataSources, FetchQuestionsRequest fetchQuestionsRequest) {
        if (CollectionUtils.isEmpty(fetchQuestionsRequest.getFilter())) {
            return dataSources;
        }

        return dataSources.stream()
                .filter(ds -> fetchQuestionsRequest.getFilter().contains(ds.getQuestionSourceType()))
                .collect(Collectors.toSet());
    }

    private DataSource readManifestFileDataSource(String dataSourceUrl) {
        URL url = getUrl(dataSourceUrl);
        String dataSourceFileType = getUrlContentType(url);
        QuestionSourceType questionSourceType = QuestionSourceType.getQuestionSourceTypeByMimeType(dataSourceFileType);
        validateDataSourceType(dataSourceFileType, questionSourceType);
        return new DataSource(url, questionSourceType);
    }


    private void validateDataSourceType(String dataSourceFileType, QuestionSourceType questionSourceType) {
        if (questionSourceType == null) {
            throw new QFetcherException(QFetcherExceptionFactory.DATA_SOURCE_TYPE_IS_NOT_SUPPORTED, dataSourceFileType);
        }
    }

    private String getUrlContentType(URL url) {
        String dataSourceFileType;
        try {
            dataSourceFileType = url.openConnection().getContentType();
        } catch (IOException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_URL, url.toString());
        }
        return dataSourceFileType;
    }

    private URL getUrl(String url) {
        URL manifestUrl;
        try {
            manifestUrl = new URL(url);
        } catch (MalformedURLException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_URL, url);
        }
        return manifestUrl;
    }
}
