package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import com.chegg.amitv.qfetcher.enums.QuestionSourceType;
import com.chegg.amitv.qfetcher.exceptions.QFetcherException;
import com.chegg.amitv.qfetcher.models.Question;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class QFetcherCsvHandler extends QFetcherHandler {
    public static final int TEXT_COLUMN = 1;

    @Override
    public List<Question> fetchDataSource(URL dataSourceUrl) {
        List<Question> questions = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(dataSourceUrl.openStream()))){
            String line = "";
            line = in.readLine(); //skip the header
            while ((line = in.readLine()) != null) {
                String[] row = line.split(",");
                if (row.length > TEXT_COLUMN) {
                    questions.add(new Question(QuestionSourceType.CSV, row[TEXT_COLUMN]));
                }
            }
        } catch (IOException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_FILE, dataSourceUrl.toString());
        }

        return questions;
    }
}