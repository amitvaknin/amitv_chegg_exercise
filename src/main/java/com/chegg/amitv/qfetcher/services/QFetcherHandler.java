package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.models.Question;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;

public abstract class QFetcherHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public List<Question> fetch(URL dataSourceUrl) {
        logger.info("Start fetch questions from URL:{}", dataSourceUrl.toString());
        List<Question> questions = fetchDataSource(dataSourceUrl);
        logger.info("Finished to fetch {} questions from URL:{}", questions.size(), dataSourceUrl.toString());
        return questions;
    }

    public abstract List<Question> fetchDataSource(URL dataSourceUrl);
}
