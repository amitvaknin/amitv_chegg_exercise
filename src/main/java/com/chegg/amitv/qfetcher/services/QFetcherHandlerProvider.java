package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.enums.QuestionSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class QFetcherHandlerProvider {
    private Map<QuestionSourceType, QFetcherHandler> questionsFetchersBySourceType;

    @Autowired
    public QFetcherHandlerProvider(QFetcherCsvHandler qFetcherCsvHandler, QFetcherImageHandler qFetcherImageHandler, QFetcherJsonHandler qFetcherJsonHandler) {
        this.questionsFetchersBySourceType = new HashMap<>();
        this.questionsFetchersBySourceType.put(QuestionSourceType.CSV, qFetcherCsvHandler);
        this.questionsFetchersBySourceType.put(QuestionSourceType.IMAGE, qFetcherImageHandler);
        this.questionsFetchersBySourceType.put(QuestionSourceType.JSON, qFetcherJsonHandler);
    }

    public QFetcherHandler getHandler(QuestionSourceType questionSourceType) {
        return questionsFetchersBySourceType.get(questionSourceType);
    }

}

