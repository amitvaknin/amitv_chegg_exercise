package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import com.chegg.amitv.qfetcher.enums.QuestionSourceType;
import com.chegg.amitv.qfetcher.exceptions.QFetcherException;
import com.chegg.amitv.qfetcher.models.Question;
import com.google.cloud.vision.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class QFetcherImageHandler extends QFetcherHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public List<Question> fetchDataSource(URL dataSourceUrl) {
        List<Question> questions = new ArrayList<>();
        String imageText = detectTextGcs(dataSourceUrl);
        questions.add(new Question(QuestionSourceType.IMAGE, imageText));
        return questions;
    }

    public String detectTextGcs(URL imageUrl) {
        String detectedText = null;
        List<AnnotateImageRequest> requests = new ArrayList<>();
        Feature feat = Feature.newBuilder().setType(Feature.Type.DOCUMENT_TEXT_DETECTION).build();
        ImageSource imgSource = ImageSource.newBuilder().setImageUri(imageUrl.toString()).build();
        Image img = Image.newBuilder().setSource(imgSource).build();
        AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
        requests.add(request);


        try (ImageAnnotatorClient  client = ImageAnnotatorClient.create()) {
            BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
            List<AnnotateImageResponse> responses = response.getResponsesList();
            if (!responses.isEmpty()) {
                AnnotateImageResponse res = responses.get(0);
                if (res.hasError()) {
                    throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_FILE, imageUrl.toString());
                }

                if (!res.getTextAnnotationsList().isEmpty()) {
                    detectedText = res.getTextAnnotationsList().get(0).getDescription();
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_FILE, imageUrl.toString());
        }


        return detectedText;
    }

}
