package com.chegg.amitv.qfetcher.services;

import com.chegg.amitv.qfetcher.enums.QFetcherExceptionFactory;
import com.chegg.amitv.qfetcher.enums.QuestionSourceType;
import com.chegg.amitv.qfetcher.exceptions.QFetcherException;
import com.chegg.amitv.qfetcher.models.JsonDataSourceQuestion;
import com.chegg.amitv.qfetcher.models.JsonDataSourceQuestions;
import com.chegg.amitv.qfetcher.models.Question;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class QFetcherJsonHandler extends QFetcherHandler {
    private ObjectMapper mapper = new ObjectMapper();

    public QFetcherJsonHandler() {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public List<Question> fetchDataSource(URL dataSourceUrl) {
        List<Question> questions = new ArrayList<>();
        try {
            JsonDataSourceQuestions jsonDataSourceQuestions = mapper.readValue(dataSourceUrl, JsonDataSourceQuestions.class);
            if (jsonDataSourceQuestions != null && jsonDataSourceQuestions.getQuestions() != null) {
                for (JsonDataSourceQuestion jsonQuestion : jsonDataSourceQuestions.getQuestions()) {
                    questions.add(new Question(QuestionSourceType.JSON, jsonQuestion.getText()));
                }
            }
        } catch (IOException e) {
            throw new QFetcherException(QFetcherExceptionFactory.UNABLE_TO_READ_FILE, dataSourceUrl.toString());
        }

        return questions;
    }
}
