package com.chegg.amitv.qfetcher.models;

import com.chegg.amitv.qfetcher.enums.QuestionSourceType;

import java.util.Set;

public class FetchQuestionsRequest {
    private String manifest;
    private Set<QuestionSourceType> filter;

    public String getManifest() {
        return manifest;
    }

    public void setManifest(String manifest) {
        this.manifest = manifest;
    }

    public Set<QuestionSourceType> getFilter() {
        return filter;
    }

    public void setFilter(Set<QuestionSourceType> filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return "FetchQuestions{" +
                "manifest='" + manifest + '\'' +
                ", filter=" + filter +
                '}';
    }


}
