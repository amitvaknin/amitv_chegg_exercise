package com.chegg.amitv.qfetcher.models;

import com.chegg.amitv.qfetcher.enums.QuestionSourceType;

public class Question {
    private String value;
    private QuestionSourceType source;

    public Question() {
       super();
    }

    public Question(QuestionSourceType source, String value) {
        this.source = source;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public QuestionSourceType getSource() {
        return source;
    }

    public void setSource(QuestionSourceType source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "Question{" +
                "value='" + value + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
