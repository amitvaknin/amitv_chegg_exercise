package com.chegg.amitv.qfetcher.models;

import com.chegg.amitv.qfetcher.enums.QuestionSourceType;

import java.net.URL;
import java.util.Objects;

public class DataSource {
    private URL url;
    private QuestionSourceType questionSourceType;

    public DataSource(URL url, QuestionSourceType questionSourceType) {
        this.url = url;
        this.questionSourceType = questionSourceType;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public QuestionSourceType getQuestionSourceType() {
        return questionSourceType;
    }

    public void setQuestionSourceType(QuestionSourceType questionSourceType) {
        this.questionSourceType = questionSourceType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataSource that = (DataSource) o;
        return Objects.equals(url, that.url) &&
                questionSourceType == that.questionSourceType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, questionSourceType);
    }
}
