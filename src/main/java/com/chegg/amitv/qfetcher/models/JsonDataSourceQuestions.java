package com.chegg.amitv.qfetcher.models;

import java.util.List;

public class JsonDataSourceQuestions {
    private List<JsonDataSourceQuestion> questions;

    public List<JsonDataSourceQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<JsonDataSourceQuestion> questions) {
        this.questions = questions;
    }
}
